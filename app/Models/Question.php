<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $table = 'pertanyaan';

    protected $fillable = ['content','image','user_id','kategori_id'];

    public function answer(){
        return $this->hasMany(Answer::class, 'pertanyaan_id');
    }
}
