<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;

    protected $table = 'jawaban';

    protected $fillable = ['content','user_id','pertanyaan_id'];

    public function user(){
    return $this->belongsTo(User::class, 'user_id');
    } 

    public function question()
    {
    return $this->belongsTo(Question::class, 'pertanyaan_id');
    }
}
