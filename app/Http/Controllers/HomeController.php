<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Question;
use App\Models\Profile;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $question = Question::get();
        $category = Category::get();
        return view('home.home',['question' => $question,'category' => $category]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $question = Question::find($id);
    //     $user = User::where('id', $question->user_id)->first();
    //     $nameuser = $user->profile->name;
    //     return view('question.detail',['question' => $question, 'nameuser' => $nameuser]);
    // }
}
