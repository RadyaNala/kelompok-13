<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Answer;
use App\Models\Question;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use File;


class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answer = Answer::all();
        return view ('answer.view', ['answer' => $answer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        $question = Question::all();
        return view('answer.create',['question' => $question]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'user_id' => ' ',
            'question_id' => ' ',
        ]);

        $iduser = Auth::id();

        $answer = new Answer;

        $answer->content = $request['content'];
        $answer->user_id = $iduser;
        $answer->question_id = $request['question_id'];

        $answer->save();

        return redirect('/answer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $answer = Answer::find($id);
        $question = Question::all();

        return view('answer.edit',['answer' => $answer, 'question' => $question]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'user_id' => ' ',
            'pertanyaan_id' => ' ',
        ]);

        $answer = Answer::find($id);

        $iduser = Auth::id();

        $answer->content = $request['content'];
        $answer->user_id = $iduser;
        $answer->pertanyaan_id = $id;

        $answer->save();

        return redirect('/answer')->with('success', 'Edit Answer Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer = Answer::find($id);
        $answer->delete();

        return redirect('/answer')->with('toast_warning', 'Delete Answer Success');
    }

    public function tambah(Request $request, $id){
        $request->validate([
            'content' => 'required',
        ]);

        $iduser = Auth::id();

        $answer = new Answer;

        $answer->content = $request->content;
        $answer->user_id = $iduser;
        $answer->pertanyaan_id = $id;

        $answer->save();

        return redirect('/question/'.$id)->with('success', 'Add Answer Success');
    }
}
