<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Question;
use App\Models\Profile;
use App\Models\User;
use App\Models\Answer;
use Illuminate\Support\Facades\Auth;
use File;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = Question::all();
        return view('question.tampil',['question' => $question]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('question.tambah',['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'user_id' => '',
            'kategori_id' => 'required',
        ]);

        $iduser = Auth::id();
        $newNameImage = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('image'), $newNameImage);
        
        $Question = new Question;
 
        $Question->content = $request['content'];
        $Question->user_id = $iduser;
        $Question->kategori_id = $request['kategori_id'];
        $Question->image = $newNameImage;
 
        $Question->save();

        return redirect('/question')->with('success', 'Add Question Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find($id);
        $user = User::where('id', $question->user_id)->first();
        $nameuser = $user->profile->name;
        return view('question.detail',['question' => $question, 'nameuser' => $nameuser]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        $category = Category::all();

        return view('question.edit',['question' => $question, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
            'user_id' => '',
            'kategori_id' => 'required',
        ]);

        $Question = Question::find($id);
        $iduser = Auth::id();
        $Question->content = $request['content'];
        $Question->user_id = $iduser;
        $Question->kategori_id = $request['kategori_id'];

        if($request->has('image')){
            $path = 'image/';
            File::delete($path. $Question->image);

            $newNameImage = time().'.'.$request->image->extension();
            $request->image->move(public_path('image'), $newNameImage);
            $Question->image = $newNameImage;
        }
        $Question->save();

        return redirect('/question')->with('success', 'Edit Question Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $path = 'image/';
        File::delete($path. $question->image);
        $question->delete();

        return redirect('/question')->with('toast_warning', 'Delete Question Success');
    }
}
