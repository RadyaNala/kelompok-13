## Final Project

## Kelompok 13

### Anggota Kelompok

- Daffa Khairon Khan
- Radya Wrahadnala Suwarno
- Wildan Nouval Rizki

## ERD

Gambar :
<img alt="ERD Project" src="erdProject.png">

## Link Video

- Link Demo Aplikasi : https://www.youtube.com/watch?v=MhetGQEv0eA
- Link Deploy : https://k13.cevy.me/