@extends('layout.dashboard')

@section('page')
    Halaman User Profile
@endsection

@section('title')
    Profile
@endsection

@section('content')
    <h1>
        {{ Auth::user()->name }}
    </h1>
    <p>
        Email : {{ Auth::user()->email }} <br>
        Umur : {{$detailProfile->age}}
    </p>

@endsection

@section('footer')
    --profile
@endsection