@extends('layout.dashboard')

@section('title')
Welcome !
@endsection

@section('content')
    @auth
    <h2>Selamat Datang, {{ Auth::user()->name }} !</h2>
    <h3>Terimakasih telah berkunjung</h3>
    @endauth      
@endsection
