@extends('layout.homemaster')
@section('title')
    Tanya.in -Register
@endsection

@section('main')
      <!-- breadcrumb Start-->
      <div class="page-notification">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Sign Up</a></li> 
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!-- Register Area Start -->
    <div class="register-form-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="register-form text-center">
                        <!-- Login Heading -->
                        <div class="register-heading">
                            <span>Sign Up</span>
                            <p>Create your account to get full access</p>
                        </div>
                        <!-- Single Input Fields -->
                        <form method="" action="">
                            @csrf
                        <div class="input-box">
                            <div class="single-input-fields">
                                <label>Full Name</label>
                                <input type="text" placeholder="Enter full name" name="name">
                            </div>
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="single-input-fields">
                                <label>Bio</label>
                                <input type="text" placeholder="Enter Bio" name="bio">
                            </div>
                            @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="single-input-fields">
                                <label>Email </label>
                                <input type="email" placeholder="Enter email" name="email">
                            </div>
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="single-input-fields">
                                <label>Age</label>
                                <input type="text" placeholder="Enter age" name="age">
                            </div>
                            @error('age')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="single-input-fields">
                                <label>Address</label>
                                <textarea cols="30" rows="10" placeholder="Enter address" name="address"></textarea>
                            </div>
                            @error('address')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="single-input-fields">
                                <label>Password</label>
                                <input type="password" placeholder="Enter Password" name="password">
                            </div>
                            @error('address')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <!-- form Footer -->
                        <div class="register-footer">
                            <p> Already have an account? <a href="/login"> Login</a></p>
                            <button class="submit-btn3">Sign Up</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Area End -->
@endsection