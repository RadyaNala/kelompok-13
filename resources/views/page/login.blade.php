@extends('layout.homemaster')
@section('title')
    Tanya.in -Login
@endsection

@section('main')
     <!-- breadcrumb Start-->
     <div class="page-notification">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Login</a></li> 
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb End -->
    <!-- login Area Start -->
    <div class="login-form-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8">
                    <div class="login-form">
                    <form action="" method="">
                        <!-- Login Heading -->
                        <div class="login-heading">
                            <span>Login</span>
                            <p>Enter Login details to get access</p>
                        </div>
                        <!-- Single Input Fields -->
                        <div class="input-box">
                            <div class="single-input-fields">
                                <label>Email Address</label>
                                <input type="text" placeholder="Email address" name="email">
                            </div>
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="single-input-fields">
                                <label>Password</label>
                                <input type="password" placeholder="Enter Password" name="password">
                            </div>
                            @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <!-- form Footer -->
                        <div class="login-footer">
                            <p>Don’t have an account? <a href="/register"> Sign Up</a></p>
                            <button class="submit-btn3">Login</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login Area End -->
@endsection