@extends('layout.dashboard')
@section('page')
Halaman Question
@endsection

@section('title')
List Question
@endsection
@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css"/>
@endpush
@push('scripts')
<script src="{{asset('/dashboardtemplate/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/dashboardtemplate/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#questionTables").DataTable();
  });
</script>
@endpush

@section('content')
    @auth
    <a href="/question/create" class="btn btn-primary btn-sm mb-3">Tambah Question</a>
    @endauth      
    <table id="questionTables" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Question</th>
          <th scope="col">Image</th>
          <th scope="col"><center>Opsi</center></th>
        </tr>
      </thead>
      <tbody>
        @forelse ($question as $key => $value)
          <tr>
            <td>{{$key +1}}</td>
            <td>{{Str::limit($value->content,30)}}</td>
            <td>
                <img src="{{asset('/image/'.$value->image)}}" height="50px">
            </td>
            <td>
              <center>
                <form action="/question/{{$value->id}}" method='post'>
                  @csrf
                  @method('delete')
                  <a href="/question/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                  @auth
                  <a href="/question/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  @endauth
                </form>
              </center>
            </td>
          </tr>
        @empty
            <tr>
              <td>Data Question Kosong</td>
            </tr>
        @endforelse
      </tbody>
    </table>
@endsection
