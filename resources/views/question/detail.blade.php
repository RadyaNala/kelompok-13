@extends('layout.dashboard')

@section('page')
    Halaman Detail Question
@endsection

@section('title')
    Question :
@endsection

@section('content')
<h1>{{$question->content}}</h1>
<p>Questioner : {{$nameuser}}</p>
<img src="{{asset('/image/'.$question->image)}}" class="w-25">

<hr>

@auth
<h1>Jawaban</h1>
@forelse ($question->answer as $item)
<div class="media my-3 border p-3">
	<div class="media-body">
		<h5 class="mt-0 text-primary">{{$item->user->name}}</h5>
        <p>{{$item->content}}</p>
	</div>
</div>
@empty
    <h4>Belum ada Jawaban</h4>
@endforelse
<br><br><hr>
<form action="/answer/{{$question->id}}" method="post">
    @csrf
    <textarea name="content" cols="30" class="form-control my-3 konten" placeholder="Please Answer" rows="10"></textarea>
    @error('content')
        <div class="alert alert-danger">
            {{$message}}
        </div>
    @enderror
    <input type="submit" value="Answer">
</form>
{{-- <a href="/answer/create" class="btn btn-primary btn-sm">Tambahkan Jawaban</a> --}}
<br><br>
<hr>
@section('link')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.3.1/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: ".konten",
        forced_root_block : "false",
        height: 350,
    })
</script>
@endsection 
@endauth

<a href="/question" class="btn btn-secondary btn-sm">Kembali</a>
@endsection
@section('link')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.3.1/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: ".konten",
        forced_root_block : "false",
        height: 350,
    })
</script>
@endsection 