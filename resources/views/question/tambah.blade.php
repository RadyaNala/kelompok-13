@extends('layout.dashboard')

@section('page')
    Halaman Tambah Question
@endsection

@section('title')
    Tambah Question
@endsection

@section('content')
<form action="/question" method="POST" enctype="multipart/form-data">
    @csrf
    {{-- pertanyaan --}}
    <div class="form-group">
        <label>Question</label>
        <textarea name="content" rows="10" cols="30" class="form-control pertanyaan"></textarea>
      </div>
      @error('content')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      {{-- category --}}
    <div class="form-group">
        <label>Category</label>
        <select name="kategori_id" class="form-control">
            <option value="">--Pilih Category--</option>
            @forelse ($category as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @empty
                <option value="">Belum ada data Category</option>
            @endforelse
        </select>
      </div>
      @error('kategori_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      {{-- userid --}}
      {{-- <div class="form-group">
        <label>user id</label>
        <input type="text" class="form-control" name="user_id">
      </div>
      @error('user_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror --}}
    {{-- <div class="form-group">
        <label>User</label>
        <select name="user_id" class="form-control">
            <option value="">--Pilih Category--</option>
            @forelse ($category as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @empty
                <option value="">Belum ada data Category</option>
            @endforelse
        </select>
      </div>
      @error('kategori_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror --}}
      {{-- image --}}
    <div class="form-group">
      <label>Image Question</label>
      <input type="file" class="form-control" name="image">
    </div>
    @error('image')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

@section('footer')
    isi footer
@endsection
@section('link')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.3.1/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: ".pertanyaan",
        forced_root_block : "false",
        height: 350,
    })
</script>
@endsection 