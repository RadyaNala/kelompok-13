    <!-- ? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{asset('/hometemplate/assets/img/logo/logotanyain.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div> 
    <!-- Preloader Start-->
    <header>
        <!-- Header Start -->
       <div class="header-area">
            <div class="main-header ">
                <div class="header-top ">
                   <div class="container-fluid">
                       <div class="col-xl-12">
                            <div class="row d-flex justify-content-lg-between align-items-center">
                                {{-- <div class="header-info-left">
                                    <li class="d-none d-lg-block">
                                        <div class="form-box f-right ">
                                            <input type="text" name="Search" placeholder="Search your interest...">
                                            <div class="search-icon">
                                                <i class="ti-search"></i>
                                            </div>
                                        </div>
                                     </li
                                </div> --}}
                                <div class="header-info-mid">
                                    <!-- logo -->
                                    <div class="logo">
                                        <a href="/home"><img src="{{('/hometemplate/assets/img/logo/logotanyain1.jpg')}}" alt=""></a>
                                    </div>
                                </div>
                                <div class="header-info-right d-flex align-items-center">
                                   <ul> 
                                        @auth
                                        <li>
                                            <a href="/dashboard" class="d-block"><p>Halo {{ Auth::user()->name }} !</p></a>
                                        </li>
                                        @endauth
                                        @guest
                                        <li>
                                            {{-- <a href="/profile" class="d-block">Belum Login</a> --}}
                                        </li>
                                        @endguest
                                        @guest
                                        <li><a href="/login">Log In  or  Sign Up</a></li>
                                        @endguest                                          
                                        @auth
                                        <li>
                                            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                                <p>
                                                Log Out
                                                </p>
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                        </li> 
                                         @endauth
                                   </ul>
                                   <!-- Social -->
                                   {{-- <div class="header-social">
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="https://bit.ly/sai4ull"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    </div> --}}
                                </div>
                            </div>
                       </div>
                   </div>
                </div>
               <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <!-- logo 2 -->
                                <div class="logo2">
                                    <a href="/home"><img src="{{('/hometemplate/assets/img/logo/logotanyain1.jpg')}}" alt=""></a>
                                </div>
                                <!-- logo 3 -->
                                <div class="logo3 d-block d-sm-none">
                                    <a href="/home"><img src="{{('/hometemplate/assets/img/logo/logotanyain1.jpg')}}" alt=""></a>
                                </div>
                            </div> 
                        </div>
                    </div>
               </div>
            </div>
       </div>
        <!-- Header End -->
    </header>