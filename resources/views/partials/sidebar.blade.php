<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('/dashboardtemplate/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        @auth
        <a href="/profile" class="d-block">{{ Auth::user()->name }}</a>
        @endauth
        @guest
        <a href="/profile" class="d-block">Belum Login</a>
        @endguest
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>
            <p>
              Menu
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/question" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>My Questions</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/answer" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>My Answers</p>
              </a>
            </li>
            @auth
            <li class="nav-item">
              <a href="/category" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>My Categories</p>
              </a>
            </li>
            @endauth
          </ul>
        </li>

        @guest
            <li class="nav-item bg-primary">
              <a href="/login" class="nav-link">
                <p>login</p>
              </a>
            </li>
        @endguest

        @auth
        <li class="nav-item bg-danger">
          <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
            <i class="nav-icon fas fa-power-off"></i>
            <p>
              Log Out
            </p>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
        </li>
        @endauth
        
       
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>