@extends('layout.dashboard')
@section('page')
Halaman Jawaban
@endsection

@section('title')
Jawaban Anda
@endsection

@section('content')
    @auth
    {{-- <a href="/Answer/create" class="btn btn-primary btn-sm mb-3">Tambah Jawaban</a> --}}
    @endauth      
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Question</th>
          <th scope="col">User</th>
          <th scope="col">Answer</th>
          <th scope="col"><center>Aksi</center></th>
        </tr>
      </thead>
      <tbody>
        @forelse ($answer as $key => $value)
          <tr>
            <td>{{$key +1}}</td>
            <td>{{$value->question->content}}</td>
            <td>{{$value->user->name}}</td>
            <td>{{$value->content}}</td>
            <td>
              <center>
                <form action="/answer/{{$value->id}}" method='post'>
                  @csrf
                  @method('delete')
                  @auth
                  <a href="/answer/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  @endauth
                </form>
              </center>
            </td>
          </tr>
        @empty
            <tr>
              <td>Data Jawaban Kosong</td>
            </tr>
        @endforelse
      </tbody>
    </table>
@endsection
