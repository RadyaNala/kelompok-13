@extends('layout.dashboard')
@section('page')
    Halaman Edit Answer
@endsection

@section('title')
    Question - Answer
@endsection

@section('content')
    <form method="POST" action="/answer/{{$answer->id}}">
        @csrf
        @method('put')

        <div class="form-group">
            <label>Jawaban Anda</label>
            <textarea name="content" cols="30" class="form-control my-3" placeholder="Please Answer" rows="10">{{$answer->content}}</textarea>
            </div>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
