@extends('layout.master')
@section('page')
Halaman Tambah Jawaban
@endsection

@section('title')
Tambah Jawaban
@endsection

@section('content')
    <form method="POST" action="/question/{{$question->id}}">
        @csrf
        
        <h2>{{$question->content}}</h2>

        <div class="form-group">
            <label>Jawaban Anda</label>
            <input type="text" name="jawaban" class="form-control" placeholder="Masukan Jawaban Anda">
            </div>
            @error('jawaban')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
