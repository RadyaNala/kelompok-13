@extends('layout.dashboard')
@section('page')
Halaman Kategori
@endsection

@section('title')
List Kategori
@endsection
@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css"/>
@endpush
@push('scripts')
<script src="{{asset('/dashboardtemplate/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/dashboardtemplate/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#categoryTables").DataTable();
  });
</script>
@endpush
@section('content')
    @auth
    <a href="/category/create" class="btn btn-primary btn-sm mb-3">Tambah Kategori</a>
    @endauth      
    <table id="categoryTables" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Kategori</th>
          <th scope="col"><center>Opsi</center></th>
        </tr>
      </thead>
      <tbody>
        @forelse ($category as $key => $value)
          <tr>
            <td>{{$key +1}}</td>
            <td>{{$value->name}}</td>
            <td>
              <center>
                <form action="/category/{{$value->id}}" method='post'>
                  @csrf
                  @method('delete')
                  <a href="/category/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                  @auth
                  <a href="/category/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  @endauth
                </form>
              </center>
            </td>
          </tr>
        @empty
            <tr>
              <td>Data Category Kosong</td>
            </tr>
        @endforelse
      </tbody>
    </table>
@endsection
