@extends('layout.dashboard')
@section('page')
Halaman Detail Kategori
@endsection

@section('title')
Detail Kategori
@endsection

@section('content')
<h1>{{$category->name}}</h1>
<p>{{$category->description}}</p>
<a href="/category" class="btn btn-secondary btn-sm">Kembali</a>
@endsection