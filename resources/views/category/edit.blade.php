@extends('layout.dashboard')
@section('page')
Halaman Edit Kategori
@endsection

@section('title')
Edit Kategori
@endsection

@section('content')
<form action="/category/{{$category->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
    <label for="kategori">Nama Kategori</label>
    <input type="text" class="form-control" id="kategori" name='name' value="{{$category->name}}">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="description">Deskripsi</label>
    <textarea name="description" id="description" cols="30" rows="10" class="form-control formcategory">{{$category->description}}</textarea>
  </div>
  @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
@section('link')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.3.1/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: ".formcategory",
        forced_root_block : "false",
        height: 350,
    })
</script>
@endsection 
