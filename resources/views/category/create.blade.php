@extends('layout.dashboard')
@section('page')
Halaman Tambah Kategori
@endsection

@section('title')
Tambah Kategori
@endsection

@section('content')
<form action="/category" method="POST">
  @csrf
  <div class="form-group">
    <label for="kategori">Nama Kategori</label>
    <input type="text" class="form-control" id="kategori" name='name'>
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="deskripsiKategori">Deskripsi</label>
    <textarea name="description" id="deskripsiKategori" cols="30" rows="10" class="form-control formcategory"></textarea>
  </div>
  @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
@section('link')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.3.1/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: ".formcategory",
        forced_root_block : "false",
        height: 350,
    })
</script>
@endsection 