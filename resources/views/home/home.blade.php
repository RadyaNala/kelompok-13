@extends('layout.homemaster')
@section('title')
    Tanya.in -Home
@endsection

@section('main')
     <!-- Top Posts Start -->
     <div class="top-post-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-tittle mb-35">
                        <h2>All Question</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-lg-between">
                <div class="col-lg-3 col-md-3">
                    <div class="list-group">
                        <a href="#" class="list-group-item disabled">
                          Category :
                        </a>
                        @forelse ($category as $key => $item)
                            <a href="category" class="list-group-item list-group-item-action">{{$item->name}}</a>
                        @empty
                            <a href="category" class="list-group-item list-group-item-action">Tidak ada Category</a>
                        @endforelse
                      </div>
                </div>
                <div class="col-lg-8 col-md-8">
                    <!-- single-job-content -->
                    @forelse ($question as $key => $value)
                    <div class="single-job-items mb-30">
                        <div class="job-items">
                            <div class="company-img">
                                <a href="#"><img src="{{asset('/image/'.$value->image)}}" alt="" width="100px"></a>
                            </div>
                            <div class="job-tittle">
                                <?php
                                    $namecategory = DB::table('kategori')
                                            ->where('id', $value->kategori_id)
                                            ->get('name');
                                ?>
                                @forelse ($namecategory as $key => $item)
                                <a href="category"><span>{{$item->name}}</span></a>
                                @empty
                                    <a href="category" class="list-group-item list-group-item-action">Tidak ada Category</a>
                                @endforelse
                                
                                <a href="/question/{{$value->id}}"><h4>{{Str::limit($value->content,30)}}</h4></a>
                            </div>
                        </div>
                    </div>
                    @empty
                    <div class="single-job-items mb-30">
                        <div class="job-items">
                            <div class="company-img">
                                <a href="#"><img src="" alt=""></a>
                            </div>
                            <div class="job-tittle">
                                <span>Kosong</span>
                                <a href="post_details.html"><h4>Kosong</h4></a>
                            </div>
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    <!-- Top Posts End -->
@endsection