@extends('layout.dashboard')
@section('page')
Halaman Edit Profile
@endsection

@section('title')
Edit Profile
@endsection

@section('content')
<form action="/profile/{{$detailProfile->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name='name' value="{{$detailProfile->name}}">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" name='email' value="{{$detailProfile->email}}">
  </div>
  @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="age">Age</label>
    <input type="number" class="form-control" id="age" name='age' value="{{$detailProfile->age}}">
  </div>
  @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="adress">Alamat</label>
    <textarea name="address" id="address" cols="30" rows="10" class="form-control">{{$detailProfile->address}}</textarea>
  </div>
  @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="bio">Biodata</label>
    <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{$detailProfile->bio}}</textarea>
  </div>
  @error('biodata')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

