@extends('layout.dashboard')

@section('page')
    Halaman User Profile
@endsection

@section('title')
    Profile
@endsection

@section('content')
    <h1>
        {{ Auth::user()->name }}
    </h1>
    <p>
        Email : {{ Auth::user()->email }} <br>
        Umur : {{$detailProfile->age}} <br>
        Alamat : {{$detailProfile->address}} <br>
        Biodata : {{$detailProfile->bio}}
    </p>
    <a href="/profile/{profile->id}/edit" class="btn btn-secondary ">Edit</a>

@endsection

@section('footer')
    --profile
@endsection