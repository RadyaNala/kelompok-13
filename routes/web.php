<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\HomeController;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Question;
use App\Models\User;
use App\Models\Category;
use App\Models\Profile;
use App\Models\Answer;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    $question = Question::get();
    $category = Category::get();
    return view('home.home',['question' => $question,'category' => $category]);
});

Route::get('/login', function () {
    return view('page.login');
});

Route::get('/register', function () {
    return view('page.register');
});

Route::get('/dashboard', function () {
    return view('page.dashboardindex');
});
    
    
//Profile
Route::resource('profile',ProfileController::class);

//Laravel Auth
Auth::routes();

//CRUD Category
Route::resource('category',CategoryController::class);

//route pertanyaan
Route::resource('question', QuestionController::class);

//CRUD Answer
Route::resource('answer',AnswerController::class);
Route::post('/answer/{pertanyaan_id}',[AnswerController::class, 'tambah']);
//
Route::resource('home',HomeController::class);
